import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(new MaterialApp(
    home: new Home(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // @override
  // Widget build(BuildContext context) {
  //   return new Scaffold(
  //     body: new Container(
  //       decoration: new BoxDecoration(
  //           gradient: new LinearGradient(
  //               begin: FractionalOffset.topCenter,
  //               end: FractionalOffset.bottomCenter,
  //               colors: [
  //             Colors.white,
  //             Colors.purpleAccent,
  //             Colors.deepPurple
  //           ])),
  //     ),
  //   );
  // }

  final List<String> gambar = [
    "spbu.jpg",
    "wy.jpg",
  ];
  static const Map<String, Color> colors = {
    '1': Color(0xFF2DB569),
    '2': Color.fromARGB(255, 181, 45, 45),
  };
  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return new Scaffold(
      body: new Container(
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Colors.white,
                  Colors.purple,
                  Color.fromARGB(255, 15, 37, 235)
                ]),
          ),
          child: new PageView.builder(
              controller: new PageController(viewportFraction: 0.8),
              itemCount: gambar.length,
              itemBuilder: (BuildContext context, int i) {
                return new Padding(
                    padding: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 50.0),
                    child: new Material(
                      elevation: 8.0,
                      child: new Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          new Hero(
                            tag: gambar[i],
                            child: new Material(
                              child: new InkWell(
                                child: new Flexible(
                                  flex: 1,
                                  child: Container(
                                    color: colors.values.elementAt(i),
                                    child: new Image.asset(
                                      "img/${gambar[i]}",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (BuildContextcontext) =>
                                            new Halamandua(
                                              gambar: gambar[i],
                                              colors:
                                                  colors.values.elementAt(i),
                                            ))),
                              ),
                            ),
                          )
                        ],
                      ),
                    ));
              })),
    );
  }
}

class Halamandua extends StatefulWidget {
  Halamandua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  State<Halamandua> createState() => _HalamanduaState();
}

class _HalamanduaState extends State<Halamandua> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Saya Senang"),
          backgroundColor: Colors.purpleAccent,
        ),
        body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                  gradient:
                      new RadialGradient(center: Alignment.center, colors: [
                Color.fromARGB(255, 97, 97, 97),
                Color.fromARGB(255, 255, 255, 255),
                Color.fromARGB(255, 115, 135, 245)
              ])),
            ),
            new Center(
              child: new Hero(
                tag: widget.gambar,
                child: new ClipOval(
                  child: new SizedBox(
                    width: 200.0,
                    height: 200.0,
                    child: new Material(
                      child: new InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: new Flexible(
                          flex: 1,
                          child: Container(
                            color: widget.colors,
                            child: new Image.asset(
                              "img/${widget.gambar}",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
